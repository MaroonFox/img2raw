from PIL import Image
import argparse
import os.path
import sys

# Parse arguments

parser = argparse.ArgumentParser(description='Exports image to grayscaled 8-bit RAW')
parser.add_argument('-i', '--input', type=str, metavar='image', default="input.png", help='input image ("input.png" by default)')
parser.add_argument('-o', '--output', type=str, metavar='raw', default="output.raw", help='output RAW ("output.raw" by default)')
parser.add_argument('-v', '--verbosity', action='count')
args = parser.parse_args()

# Exit if input file doesn't exist

if (not os.path.isfile(args.input)):
    
    if (args.verbosity != None and args.verbosity > 0):
        print(f'Input file {args.input} doesn\'t exist!')

    sys.exit(-1)

# Load image

image = Image.open(args.input)
grayscaled_image = image.convert('L')
width, height = image.size

# Print info if needed

if (args.verbosity != None and args.verbosity > 0):
    print(f'{args.input} is {width}×{height}')
    print(f'Output file is {args.output}')

# Output

output = open(args.output,'wb')

for h in range(0, height):
    
    for w in range(0, width):
        pixel_int = grayscaled_image.getpixel((h, w))
        output.write(bytearray([pixel_int]))

image.close()
output.close()